import { Component, Input, OnInit } from "@angular/core";

import { PokeResult } from "../models";

@Component({
  selector: "app-pokemon-list-item",
  templateUrl: "./pokemon-list-item.component.html",
  styleUrls: ["./pokemon-list-item.component.css"]
})
export class PokemonListItemComponent implements OnInit {
  @Input() pokemon: PokeResult;
  id: number;

  constructor() { }

  ngOnInit() {
    this.id = +this.pokemon.url.split("/")[6];
  }

  getName() {
    return this.pokemon.name.toUpperCase();
  }

  getUrl() {
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.id}.png`;
  }

}
