import { Component, OnInit, Input } from "@angular/core";

import { Pokemon, PokeMove } from "../models";

@Component({
  selector: "app-pokemon-moves",
  templateUrl: "./pokemon-moves.component.html",
  styleUrls: ["./pokemon-moves.component.css"]
})
export class PokemonMovesComponent implements OnInit {
  @Input() pokemon: Pokemon;
  moves: PokeMove[];

  constructor() { }

  ngOnInit() {
    this.moves = this.pokemon.moves;
    this.formatMoves();
  }

  formatMoves() {
    for (const move of this.moves) {
      move.move.name = move.move.name.replace(/-/g, " ");
    }
  }
}
