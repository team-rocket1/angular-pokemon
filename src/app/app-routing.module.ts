import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PokemonListComponent } from "./pokemon-list/pokemon-list.component";
import { PokemonDetailComponent } from "./pokemon-detail/pokemon-detail.component";

const routes: Routes = [
  { path: "page/:num", component: PokemonListComponent },
  { path: "pokemon/:id", component: PokemonDetailComponent },
  { path: "", redirectTo: "page/1", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
