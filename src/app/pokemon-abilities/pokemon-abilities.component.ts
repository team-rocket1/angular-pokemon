import { Component, OnInit, Input } from "@angular/core";

import { Pokemon } from "../models";

@Component({
  selector: "app-pokemon-abilities",
  templateUrl: "./pokemon-abilities.component.html",
  styleUrls: ["./pokemon-abilities.component.css"]
})
export class PokemonAbilitiesComponent implements OnInit {
  @Input() pokemon: Pokemon;

  constructor() { }

  ngOnInit() {
  }

}
