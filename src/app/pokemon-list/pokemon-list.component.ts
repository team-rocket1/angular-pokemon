import { Component, OnInit} from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { PokemonService } from "../pokemon.service";
import { Pokelist } from "../models";

@Component({
  selector: "app-pokemon-list",
  templateUrl: "./pokemon-list.component.html",
  styleUrls: ["./pokemon-list.component.css"],
})
export class PokemonListComponent implements OnInit {
  pokemons: Pokelist;

  constructor(
    private pokemonService: PokemonService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getPokelist();
  }
  update(action: number) {
    switch (action) {
      case 0:
        this.getNextPokelist(1);
        break;
      case -1:
        this.getNextPokelist(this.getPageNr() - 1);
        break;
      case 1:
        this.getNextPokelist(this.getPageNr() + 1);
        break;
      default:
        this.getNextPokelist(action);
    }
  }

  getPageNr(): number {
    return +this.route.snapshot.paramMap.get("num");
  }

  pageIs(page: number): boolean {
    if (this.getPageNr() === page) {
      return true;
    } else {
      return false;
    }
  }

  getPokelist(): void {
    const page = +this.route.snapshot.paramMap.get("num");
    this.pokemonService.getPokelist(page).subscribe(pokemons => this.pokemons = pokemons);
  }
  getNextPokelist(page: number): void {
    this.pokemonService.getPokelist(page).subscribe(pokemons => this.pokemons = pokemons);
  }
}
