import { Component, OnInit, Input } from "@angular/core";

import { Pokemon, PokeSprites } from "../models";

@Component({
  selector: "app-pokemon-sprites",
  templateUrl: "./pokemon-sprites.component.html",
  styleUrls: ["./pokemon-sprites.component.css"]
})
export class PokemonSpritesComponent implements OnInit {
  @Input() pokemon: Pokemon;
  sprites: PokeSprites;

  constructor() { }

  ngOnInit() {
    this.sprites = this.pokemon.sprites;
  }
}
