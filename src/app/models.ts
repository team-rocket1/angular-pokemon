export interface Pokelist {
  count: number;
  next?: string;
  previous?: string;
  results: PokeResult[];
}

export interface PokeResult {
  name: string;
  url: string;
}

export interface Pokemon {
  // Profile
  name: string;
  types: PokeType[];
  height: number;
  weight: number;
  base_experience: number;

  // Base stats
  stats: PokeStat[];

  // Abilities
  abilities: PokeAbility[];

  // Sprites
  sprites: PokeSprites;

  // Moves
  moves: PokeMove[];
}

export interface PokeType {
  slot: number;
  type: {
    name: string
  };
}

export interface PokeStat {
  base_stat: number;
  stat: {
    name: string
  };
}

export interface PokeAbility {
  ability: {
    name: string
  };
  slot: number;
}

export interface PokeSprites {
  front_default: string;
  front_female?: string;
  front_shiny?: string;
  front_shiny_female?: string;
  back_default?: string;
  back_female?: string;
  back_shiny?: string;
  back_shiny_female?: string;
}

export interface PokeMove {
  move: {
    name: string;
  };
}
