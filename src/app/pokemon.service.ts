import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

import { Pokemon, Pokelist } from "./models";

@Injectable({
  providedIn: "root"
})
export class PokemonService {
  private baseUrl = "https://pokeapi.co/api/v2/";

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private http: HttpClient) { }

  getPokelist(page: number): Observable<Pokelist> {
    page = ((page - 1) * 20);
    return this.http.get<Pokelist>(`${this.baseUrl}pokemon/?offset=${page}&limit=20`);
  }

  getPokemon(id: number) {
    return this.http.get<Pokemon>(`${this.baseUrl}pokemon/${id}`);
  }
}
