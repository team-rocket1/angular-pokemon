# AngularPokemon
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![heroku](https://img.shields.io/static/v1?logo=gitlab&message=view&label=heroku&color=e24329)](https://team-rocket-pokedex.herokuapp.com/)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.24.

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Build](#build)
- [Components](#components)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install
```
npm install
```

## Development server
```
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
```

## Build
```
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
```

## Components

The project has, apart from the root app component, two parent components, each with various child components.

### Pokemon-list

Lists all pokemons by displaying the pokemon-list-item component.

### Pokemon-detail

Lists all details of a specific pokemon by displaying the components pokemon-profile, pokemon-stats, pokemon-abilities, pokemon-sprites and pokemon-moves.

## Maintainers

[Marius S. Lode @Marius-L](https://gitlab.com/Marius-L)  
[Sondre Elde @@sondreelde](https://gitlab.com/sondreelde)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT (c) Marius S. Lode, Sondre Elde
