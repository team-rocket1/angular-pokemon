"use strict";
/**
 * Dependencies
 * @ignore
 */
const path = require("path");
const express = require("express");
//const history = require("connect-history-api-fallback");
/**
 * Dotenv
 * @ignore
 */
require("dotenv").config();
/**
 * Express
 * @ignore
 */
const { PORT: port = 3000 } = process.env;
const app = express();
app.use(express.static(path.join("dist/angular-pokemon")));
/**
 * Launch app
 * @ignore
 */

// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
